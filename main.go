package main

import (
	"Demo/source/db"
	"Demo/source/handlers"
	"Demo/source/keys"
	"Demo/source/logger"
	"Demo/source/multiplexer"

	_ "github.com/mattn/go-sqlite3" // Import go-sqlite3 library
	"go.uber.org/fx"
)

// @title Demo Pooya
// @version 1.0
// @description This is a backend of Sepand documentation.

// @BasePath /api
// @host localhost:6060

// @securityDefinitions.apikey ApiKeyAuth
// @in header
// @name Authorization
func main() {

	// programmatically set swagger info
	// docs.SwaggerInfo.Title = "Spand Backend system"

	mainModules := fx.Options(db.Module,
		logger.Module,
		multiplexer.Module,
		handlers.Module,
		keys.Module)

	fx.New(mainModules).Run()

}
