package entity

import "strings"

//Karbar entity
type Karbar struct {
	ID        uint64 `json:"ID,omitempty" example:"1"`
	FirstName string `json:"firstName" example:"sina"`
	LastName  string `json:"lastName,omitempty" example:"irani"`
	Password  string `json:"password,omitempty" example:"pass"`
	Image     string `json:"image,omitempty" example:"imageURL"`
	CreatedAt string `json:"createdAt,omitempty" example:"2020-08-07 14:46:25"`
	UpdatedAt string `json:"updatedAt,omitempty" example:"2020-08-07 14:46:25"`
}

//Fields will return all fields of this type
func (k *Karbar) Fields() []interface{} {
	return []interface{}{
		&k.ID,
		&k.FirstName,
		&k.LastName,
		&k.Password,
		&k.Image,
		&k.CreatedAt,
		&k.UpdatedAt,
	}
}

//TableName will name of table in database
func (k *Karbar) TableName() string {
	return "Karbar"
}

//KarbarColumns send all columns in string
func KarbarColumns() string {
	return "ID,firstName,lastName,password,image,createdAt,updatedAt"
}

//KarbarColumnsList send all columns in string
func KarbarColumnsList() []string {
	columns := KarbarColumns()
	colList := strings.Split(columns, ",")
	return colList
}
