package config

// BaseAPIAdd is base address of all apis
var BaseAPIAdd = "/api"

// BuildMode is build mode
var BuildMode = "Dev" //values: Dev Pro
