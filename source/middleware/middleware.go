package middleware

import (
	"Demo/source/keys"
	"Demo/source/service/systemuserservice"

	"github.com/gin-gonic/gin"
	"go.uber.org/fx"
	"go.uber.org/zap"
)

// Module is the Profile handler fx module.
var Module = fx.Options(
	fx.Provide(NewMiddleware),
)

// Middleware is the interface for the ping handler.
type Middleware interface {
	Middleware(nextHandler gin.HandlerFunc) gin.HandlerFunc
}

// Params is the input parameter struct for the module that contains its dependencies
type Params struct {
	fx.In

	Logger            *zap.Logger
	SystemUserService systemuserservice.SystemUserService
	Keys              keys.Keys
}

type middleware struct {
	logger            *zap.Logger
	systemUserService systemuserservice.SystemUserService
	keys              keys.Keys
}

// NewMiddleware constructs a new Middleware.
func NewMiddleware(p Params) (Middleware, error) {

	middlewareItem := &middleware{
		logger:            p.Logger,
		systemUserService: p.SystemUserService,
		keys:              p.Keys,
	}

	return middlewareItem, nil

}

func (m *middleware) Middleware(nextHandler gin.HandlerFunc) gin.HandlerFunc {

	return func(c *gin.Context) {

		nextHandler(c)
	}
}
