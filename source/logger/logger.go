package logger

import (
	"fmt"
	"log"
	"os"

	"go.uber.org/fx"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
	"gopkg.in/natefinch/lumberjack.v2"
)

//Module is one main modules
var Module = fx.Provide(NewLogger)

//NewLogger create new Logger type
func NewLogger() (logger *zap.Logger, err error) {

	rootPath, err := os.Getwd()

	if err != nil {
		log.Println(err)
		return nil, err
	}

	_, err = os.Open(rootPath + "/Demo.log")

	if err != nil {
		_, err = os.Create(rootPath + "/Demo.log")
	}

	if err != nil {
		log.Println(err)
		return nil, err
	}

	var lumlog = &lumberjack.Logger{
		Filename:   rootPath + "/Demo.log",
		MaxSize:    1,  // megabytes
		MaxBackups: 10, // number of log files
		MaxAge:     1,  // days
		LocalTime:  true,
	}

	writeFunc := func(e zapcore.Entry) error {
		lumlog.Write([]byte(fmt.Sprintf("%+v\n", e)))
		return nil
	}

	logger, err = zap.NewProduction(zap.Hooks(writeFunc))

	if err != nil {
		log.Fatalf("couldn't create *zap.Logger dependency")
		return nil, err
	}

	defer logger.Sync()

	return logger, nil

}
