package keys

import (
	"crypto/rand"
	"crypto/rsa"

	"go.uber.org/fx"
	"go.uber.org/zap"
)

//Params is the input parameter struct for the module that contains its dependencies
type Params struct {
	fx.In

	Logger *zap.Logger
}

//Module is the Profile handler fx module.
var Module = fx.Options(
	fx.Provide(NewKeys),
)

//Keys is struct for keeping public and private keys
type Keys struct {
	PrivateKey *rsa.PrivateKey
	PublicKey  *rsa.PublicKey
}

//NewKeys create new Key
func NewKeys() (Keys, error) {

	privKey, err := rsa.GenerateKey(rand.Reader, 2048)

	if err != nil {
		return Keys{}, err
	}

	keysItem := Keys{
		PrivateKey: privKey,
		PublicKey:  &privKey.PublicKey,
	}

	return keysItem, nil
}
