package db

import (
	"context"
	"database/sql"
	"os"

	"go.uber.org/fx"
	"go.uber.org/zap"
	"golang.org/x/crypto/bcrypt"
)

//DIParam is all dependencies needed to create database connection
type DIParam struct {
	fx.In

	Logger *zap.Logger
}

// Module is all di needed for db connection
var Module = fx.Options(
	fx.Provide(NewDBConn),
	fx.Invoke(testDBConn),
	fx.Invoke(initializeDB),
)

//NewDBConn is provider for type *Pool
func NewDBConn(param DIParam) (db *sql.DB, err error) {

	dbFile, err := os.Open("sepand.db") // Create SQLite file

	if err != nil {
		param.Logger.Error(err.Error())
		dbFile, err = os.Create("sepand.db")
		if err != nil {
			param.Logger.Fatal("couldn't create database file" + err.Error())
			return nil, err
		}
	}

	dbFile.Close()

	sqliteDatabase, _ := sql.Open("sqlite3", "./sepand.db") // Open the created SQLite File

	param.Logger.Info("sepand.db opened")

	// defer sqliteDatabase.Close()                                     // Defer Closing the database

	return sqliteDatabase, nil

}

type tblInfoStruct struct {
	cid          string
	name         string
	typeOf       string
	notnull      string
	defaultValue sql.NullString
	pk           string
}

func (t *tblInfoStruct) Fields() []interface{} {
	return []interface{}{
		&t.cid,
		&t.name,
		&t.typeOf,
		&t.notnull,
		&t.defaultValue,
		&t.pk,
	}
}

type param struct {
	fx.In

	DB     *sql.DB
	Logger *zap.Logger
}

func testDBConn(par param) (err error) {

	rows, err := par.DB.Query(`
	SELECT
		name
	FROM
		sqlite_master
	WHERE
		type ='table' AND
		name NOT LIKE 'sqlite_%';`)

	if err != nil {
		par.Logger.Error("Unable to connect to database: %v\n", zap.Error(err))
	}

	defer rows.Close()

	var tblName string

	for rows.Next() {
		err := rows.Scan(&tblName)
		if err != nil {
			par.Logger.Error(err.Error())
			return err
		}
		par.Logger.Info("tbl_name : " + tblName + " found.")
	}

	err = rows.Err()
	if err != nil {
		par.Logger.Error(err.Error())
		return err
	}

	par.Logger.Info("successfully connected to database .")

	return nil

}

func initializeDB(par param) (err error) {

	// initialize database **********************************
	// ******************************************************

	// Karbar table
	tx, err := par.DB.BeginTx(context.Background(), nil)

	res, err := tx.Exec(`
	CREATE TABLE IF NOT EXISTS SystemUsers (
		ID INTEGER PRIMARY KEY AUTOINCREMENT,
		firstName TEXT, 
		lastName TEXT, 
		password TEXT, 
		image TEXT, 
		createdAt TEXT, 
		updatedAt TEXT
		);
	`)
	tx.Commit()

	if err != nil {
		par.Logger.Error("couldn't create table Karbar")
		return err
	}

	count, err := res.RowsAffected()

	if count > 0 {
		par.Logger.Info("Karbar table Created.")
	}

	par.Logger.Info("inserting initial data finished successfully.\n")

	return nil
}

// Utility ---------------------------------

func hashPassword(password string) (string, error) {
	bytes, err := bcrypt.GenerateFromPassword([]byte(password), 12)
	return string(bytes), err
}

func checkPasswordHash(password, hash string) bool {
	err := bcrypt.CompareHashAndPassword([]byte(hash), []byte(password))
	return err == nil
}
