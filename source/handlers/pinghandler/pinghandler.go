package pinghandler

import (
	"Demo/source/config"

	"github.com/gin-gonic/gin"
	"go.uber.org/fx"
	"go.uber.org/zap"
)

//Module is the Profile handler fx module.
var Module = fx.Options(
	fx.Invoke(NewPingHandler),
)

type pingHandler struct {
	logger *zap.Logger
}

//Handler is the interface for the ping handler.
type Handler interface {
	Ping(c *gin.Context)
}

//Params is the input parameter struct for the module that contains its dependencies
type Params struct {
	fx.In

	Logger *zap.Logger
	Srv    *gin.Engine
}

//NewPingHandler constructs a new ping.Handler.
func NewPingHandler(p Params) (Handler, error) {

	pingHandlerItem := &pingHandler{
		logger: p.Logger,
	}

	p.Srv.GET(config.BaseAPIAdd+"/ping", pingHandlerItem.Ping)

	return pingHandlerItem, nil
}

// Ping godoc
// @Summary ping
// @Description ping to backend
// @Tags ping
// @Accept json
// @Produce json
// @Success 200 {string} string "pong"
// @Failure 400 {string} string "ok"
// @Failure 404 {string} string "ok"
// @Failure 500 {string} string "ok"
// @Router /ping [get]
func (p *pingHandler) Ping(c *gin.Context) {

	c.JSON(200, gin.H{
		"message": "pong",
	})

}
