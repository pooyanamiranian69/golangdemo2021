package handlers

import (
	"Demo/source/handlers/pinghandler"

	"go.uber.org/fx"
)

//Module is all di needed for all handelers
var Module = fx.Options(
	pinghandler.Module,
)
