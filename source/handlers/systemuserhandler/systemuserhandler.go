package systemuserhandler

import (
	"Demo/source/config"
	"net/http"

	"Demo/source/entity"
	"Demo/source/middleware"
	"Demo/source/service/systemuserservice"

	"github.com/gin-gonic/gin"
	"go.uber.org/fx"
	"go.uber.org/zap"
)

// Module is the System user handler fx module.
var Module = fx.Options(
	fx.Invoke(newUserHandler),
)

// Handler is the interface foe user system handler.
type Handler interface {
	insertNewSystemUser(c *gin.Context)
	authenticate(c *gin.Context)
}

type userHandler struct {
	systemuserService systemuserservice.SystemUserService
	logger            *zap.Logger
}

// Params is the input parameter struct for the module that contains its dependencies
type Params struct {
	fx.In

	SystemUserService systemuserservice.SystemUserService
	Mw                middleware.Middleware
	Logger            *zap.Logger
	Srv               *gin.Engine
}

// newUserHandler constructs a new handler.
func newUserHandler(p Params) (Handler, error) {

	mw := p.Mw.Middleware

	handler := &userHandler{
		systemuserService: p.SystemUserService,
		logger:            p.Logger,
	}

	p.Srv.POST(config.BaseAPIAdd+"/systemuser/authenticate", handler.authenticate)
	p.Srv.POST(config.BaseAPIAdd+"/systemuser/insertnew", mw(handler.insertNewSystemUser))

	return handler, nil

}

// AuthSystemUserReq type for request
type AuthSystemUserReq struct {
	UserName string `json:"username" example:"pooya"`
	Password string `json:"password" example:"12345"`
}

// authenticate godoc
// @Description Authenticate System user
// @Id system-user-authenticate
// @Tags system-user
// @Summary Authenticate System user
// @Accept application/json
// @Produce application/json
// @Failure 400 {object} []byte "Error in input body, empty fields or this user does not exist"
// @Failure 401 {object} []byte "Invalid password"
// @Failure 500 {object} []byte "Couldn't create token"
// @Router /systemuser/authenticate [post]
func (u *userHandler) authenticate(c *gin.Context) {

	c.JSON(http.StatusOK, nil)

}

// insertNewSystemUser godoc
// @Description Add new System user
// @Id system-user-insert
// @Tags system-user
// @Summary Add new System user
// @Accept application/json
// @Produce application/json
// @Produce text/plain
// @Failure 400 {object} []byte "Error in input body, empty fields, system user already exists or user type does not exist"
// @Failure 401 {object} []byte "Couldn't find user"
// @Failure 403 {object} []byte "No permission"
// @Failure 500 {object} []byte "Couldn't save the new system user"
// @Router /systemuser/insertnew [post]
func (u *userHandler) insertNewSystemUser(c *gin.Context) {
	// now do rest---------------

	var newUser entity.Karbar

	if err := c.ShouldBindJSON(&newUser); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	//validations ----------

	// check username and password exist
	if newUser.FirstName == "" || newUser.Password == "" {
		c.JSON(http.StatusBadRequest, "username and password can not be empty")
		return
	}

	// now save new system-user
	newUserSaved, err := u.systemuserService.InsertNewSystemUser(c.Request.Context(), &newUser)
	if err != nil {
		u.logger.Error(err.Error())
		c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		return
	}

	newUserSaved.Password = ""

	c.JSON(http.StatusOK, newUserSaved)

}
