package systemuserservice

import (
	"Demo/source/entity"
	"context"

	"go.uber.org/fx"
	"go.uber.org/zap"
)

// Params is the input parameter struct for the module that contains its dependencies
type Params struct {
	fx.In

	Logger *zap.Logger
}

// Module is the Profile handler fx module.
var Module = fx.Options(
	fx.Provide(NewUserService),
)

// SystemUserService is the interface for the ping handler.
type SystemUserService interface {
	InsertNewSystemUser(context.Context, *entity.Karbar) (*entity.Karbar, error)
	AuthenticateSystemUser(ctx context.Context, username string, password string) (bool, error)
}

type userService struct {
	logger *zap.Logger
}

// NewUserService constructs a new UserService.
func NewUserService(p Params) (SystemUserService, error) {

	userServiceItem := &userService{
		logger: p.Logger,
	}

	return userServiceItem, nil

}

func (u *userService) InsertNewSystemUser(ctx context.Context, user *entity.Karbar) (*entity.Karbar, error) {

	u.logger.Info("inserted new karbar")

	return user, nil
}

func (u *userService) AuthenticateSystemUser(ctx context.Context, username string, password string) (bool, error) {

	return true, nil
}
