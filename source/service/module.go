package service

import (
	"Demo/source/service/systemuserservice"

	"go.uber.org/fx"
)

// Module is all di needed for all handelers
var Module = fx.Options(
	systemuserservice.Module,
)
